import { useRouter } from 'next/router';
import React, { useEffect, useRef, useState } from 'react';
import { Button, PageHeader, Descriptions, Input, message } from 'antd';
import styled from 'styled-components';

import { withContextInitialized } from '../../components/hoc';
import CompanyCard from '../../components/molecules/CompanyCard';
import GenericList from '../../components/organisms/GenericList';
import OverlaySpinner from '../../components/molecules/OverlaySpinner';
import { usePersonInformation } from '../../components/hooks/usePersonInformation';

import { Company } from '../../constants/types';
import { ResponsiveListCard } from '../../constants';

const PersonInput = styled(Input)`
  margin-top: 1rem;
`

const PersonDetail = () => {
  const router = useRouter();
  const [editPersonInfo, setEditPersonInfo] = useState(false);
  const { load, loading, save, data } = usePersonInformation(
    router.query?.email as string,
    true
  );
  const [personData, setPersonData] = useState({
    ...data
  });

  useEffect(() => {
    load();
  }, []);

  const toggleEditPersonInfo = () => setEditPersonInfo(!editPersonInfo)

  const handleEditPersonInfoChanges = (e) => {
    setPersonData({
      ...personData,
      [e.target.name]: e.target.value
    })
  }

  if (loading) {
    return <OverlaySpinner title={`Loading ${router.query?.email} information`} />;
  }

  if (!data) {
    message.error("The user doesn't exist redirecting back...", 2, () =>
      router.push('/home')
    );
    return <></>;
  }

  const handleSubmit = async (e) => {
    e.preventDefault()
    try {
      const response = await save(personData)
      console.log(response)
      toggleEditPersonInfo()
    } catch (error) {
      message.error(error)
    }
  } 

  return (
    <>
      <PageHeader
        onBack={router.back}
        title="Person"
        subTitle="Profile"
        extra={[
          <Button
            style={{ padding: 0, margin: 0 }}
            type="link"
            href={data.website}
            target="_blank"
            rel="noopener noreferrer"
          >
            Visit website
          </Button>,
          <Button type="default" onClick={toggleEditPersonInfo}>
            {editPersonInfo ? "Cancel" : "Edit"}
          </Button>,
        ]}
      >
        {data && !editPersonInfo ? (
          <Descriptions size="small" column={1}>
            <Descriptions.Item label="Name">{data.name}</Descriptions.Item>
            <Descriptions.Item label="Gender">{data.gender}</Descriptions.Item>
            <Descriptions.Item label="Phone">{data.phone}</Descriptions.Item>

            <Descriptions.Item label="Birthday">{data.birthday}</Descriptions.Item>
          </Descriptions>
        ) : (
          <form id="form-person-info" onSubmit={handleSubmit}>
            <PersonInput addonBefore="Name" name="name" value={personData.name} onChange={handleEditPersonInfoChanges} />
            <PersonInput addonBefore="Gender" name="gender" value={personData.gender} onChange={handleEditPersonInfoChanges} />
            <PersonInput addonBefore="Phone" name="phone" value={personData.phone} onChange={handleEditPersonInfoChanges} />
            <PersonInput addonBefore="Birthday" name="birthday" type="date" value={personData.birthday} onChange={handleEditPersonInfoChanges} />

            <Input type="submit" value="Save Person Info" style={{ marginTop: "2rem" }}/>
          </form>
        )}
        <GenericList<Company>
          loading={loading}
          extra={ResponsiveListCard}
          data={data && data.companyHistory}
          ItemRenderer={({ item }: any) => <CompanyCard item={item} />}
          handleLoadMore={() => {}}
          hasMore={false}
        />
      </PageHeader>
    </>
  );
};

export default withContextInitialized(PersonDetail);
